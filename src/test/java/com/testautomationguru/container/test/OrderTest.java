package test.java.com.testautomationguru.container.test;

import main.java.com.testautomationguru.container.pages.OrderPage;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.List;

public class OrderTest extends BaseTest {

    private OrderPage store;

    @DataProvider(name = "category")
    public static Object[][] credentials() {
        return new Object[][]{
                {"Accessories"},
                {"iMacs"},
                {"iPads"},
                {"iPhones"},
                {"iPods"},
                {"MacBooks"},
                {"Accessories"},
                {"iMacs"},
                {"iPads"},
                {"iPhones"},
                {"iPods"},
                {"MacBooks"}
        };
    }

    @BeforeTest
    public void setUp() throws MalformedURLException {
        super.setUp();
        store = new OrderPage(driver);
    }

    @Test(dataProvider = "category")
    public void googleTest(String category) {
        store.goTo();
        store.goToCategory(category);
        List<String> itemPrice = store.getListOfItems();
        itemPrice.stream()
                .forEach(System.out::println);
        System.out.println("----------------------------");
        Assert.assertTrue(itemPrice.size() > 0);
    }

}
